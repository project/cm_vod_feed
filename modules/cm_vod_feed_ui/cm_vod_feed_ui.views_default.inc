<?php
/**
 * @file
 * cm_vod_feed_ui.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function cm_vod_feed_ui_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'cm_vod_find_show';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'VOD Feeds: Find Show';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'VOD Feeds: Find Show';
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Filter';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '25';
  $handler->display->display_options['style_plugin'] = 'isotope';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Header: Global: Result summary */
  $handler->display->display_options['header']['result']['id'] = 'result';
  $handler->display->display_options['header']['result']['table'] = 'views';
  $handler->display->display_options['header']['result']['field'] = 'result';
  $handler->display->display_options['header']['result']['content'] = '@total Show nodes in the System';
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = '<div class="messages warning">Now Shows with this Expected Filename</div>';
  $handler->display->display_options['empty']['area']['format'] = '2';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Expected Filename */
  $handler->display->display_options['fields']['field_expected_filename']['id'] = 'field_expected_filename';
  $handler->display->display_options['fields']['field_expected_filename']['table'] = 'field_data_field_expected_filename';
  $handler->display->display_options['fields']['field_expected_filename']['field'] = 'field_expected_filename';
  /* Field: Content: Edit link */
  $handler->display->display_options['fields']['edit_node']['id'] = 'edit_node';
  $handler->display->display_options['fields']['edit_node']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['edit_node']['field'] = 'edit_node';
  /* Field: Content: Post date */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'node';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['date_format'] = 'short';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'cm_show' => 'cm_show',
  );
  /* Filter criterion: Content: Expected Filename (field_expected_filename) */
  $handler->display->display_options['filters']['field_expected_filename_value']['id'] = 'field_expected_filename_value';
  $handler->display->display_options['filters']['field_expected_filename_value']['table'] = 'field_data_field_expected_filename';
  $handler->display->display_options['filters']['field_expected_filename_value']['field'] = 'field_expected_filename_value';
  $handler->display->display_options['filters']['field_expected_filename_value']['operator'] = 'contains';
  $handler->display->display_options['filters']['field_expected_filename_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_expected_filename_value']['expose']['operator_id'] = 'field_expected_filename_value_op';
  $handler->display->display_options['filters']['field_expected_filename_value']['expose']['label'] = 'Filename';
  $handler->display->display_options['filters']['field_expected_filename_value']['expose']['operator'] = 'field_expected_filename_value_op';
  $handler->display->display_options['filters']['field_expected_filename_value']['expose']['identifier'] = 'field_expected_filename_value';
  $handler->display->display_options['filters']['field_expected_filename_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    31 => 0,
    19 => 0,
    22 => 0,
    48 => 0,
    49 => 0,
    21 => 0,
    45 => 0,
    46 => 0,
    47 => 0,
    42 => 0,
    30 => 0,
    24 => 0,
    6 => 0,
    5 => 0,
    12 => 0,
    17 => 0,
    16 => 0,
    7 => 0,
    13 => 0,
    51 => 0,
    28 => 0,
    29 => 0,
    11 => 0,
    27 => 0,
    9 => 0,
    26 => 0,
    10 => 0,
    35 => 0,
    8 => 0,
    50 => 0,
    53 => 0,
  );
  /* Filter criterion: Broken/missing handler */
  $handler->display->display_options['filters']['field_cm_show_vod_fid']['id'] = 'field_cm_show_vod_fid';
  $handler->display->display_options['filters']['field_cm_show_vod_fid']['table'] = 'field_data_field_cm_show_vod';
  $handler->display->display_options['filters']['field_cm_show_vod_fid']['field'] = 'field_cm_show_vod_fid';
  $handler->display->display_options['filters']['field_cm_show_vod_fid']['operator'] = 'not empty';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'title' => 'title',
    'field_expected_filename' => 'field_expected_filename',
    'edit_node' => 'edit_node',
    'field_cm_show_vod' => 'field_cm_show_vod',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'title' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_expected_filename' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'edit_node' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_cm_show_vod' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'cm_show' => 'cm_show',
  );
  /* Filter criterion: Content: Expected Filename (field_expected_filename) */
  $handler->display->display_options['filters']['field_expected_filename_value']['id'] = 'field_expected_filename_value';
  $handler->display->display_options['filters']['field_expected_filename_value']['table'] = 'field_data_field_expected_filename';
  $handler->display->display_options['filters']['field_expected_filename_value']['field'] = 'field_expected_filename_value';
  $handler->display->display_options['filters']['field_expected_filename_value']['operator'] = 'contains';
  $handler->display->display_options['filters']['field_expected_filename_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_expected_filename_value']['expose']['operator_id'] = 'field_expected_filename_value_op';
  $handler->display->display_options['filters']['field_expected_filename_value']['expose']['label'] = 'Filename';
  $handler->display->display_options['filters']['field_expected_filename_value']['expose']['operator'] = 'field_expected_filename_value_op';
  $handler->display->display_options['filters']['field_expected_filename_value']['expose']['identifier'] = 'field_expected_filename_value';
  $handler->display->display_options['filters']['field_expected_filename_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    31 => 0,
    19 => 0,
    22 => 0,
    48 => 0,
    49 => 0,
    21 => 0,
    45 => 0,
    46 => 0,
    47 => 0,
    42 => 0,
    30 => 0,
    24 => 0,
    6 => 0,
    5 => 0,
    12 => 0,
    17 => 0,
    16 => 0,
    7 => 0,
    13 => 0,
    51 => 0,
    28 => 0,
    29 => 0,
    11 => 0,
    27 => 0,
    9 => 0,
    26 => 0,
    10 => 0,
    35 => 0,
    8 => 0,
    50 => 0,
    53 => 0,
  );
  $handler->display->display_options['path'] = 'manage/vod/find-show';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Find Show';
  $handler->display->display_options['menu']['weight'] = '10';
  $handler->display->display_options['menu']['name'] = 'management';
  $handler->display->display_options['menu']['context'] = 0;
  $export['cm_vod_find_show'] = $view;

  $view = new view();
  $view->name = 'cm_vod_without_matching_show';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'file_managed';
  $view->human_name = 'VOD Without Mathcing Show';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'VOD Feeds UI: Video Without Matching Show';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'role';
  $handler->display->display_options['access']['role'] = array(
    31 => '31',
    22 => '22',
  );
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'table';
  /* Header: Global: Result summary */
  $handler->display->display_options['header']['result']['id'] = 'result';
  $handler->display->display_options['header']['result']['table'] = 'views';
  $handler->display->display_options['header']['result']['field'] = 'result';
  $handler->display->display_options['header']['result']['content'] = '@total VODs in the System';
  /* Relationship: File Usage: Content */
  $handler->display->display_options['relationships']['file_to_node']['id'] = 'file_to_node';
  $handler->display->display_options['relationships']['file_to_node']['table'] = 'file_usage';
  $handler->display->display_options['relationships']['file_to_node']['field'] = 'file_to_node';
  /* Field: File: Name */
  $handler->display->display_options['fields']['filename']['id'] = 'filename';
  $handler->display->display_options['fields']['filename']['table'] = 'file_managed';
  $handler->display->display_options['fields']['filename']['field'] = 'filename';
  $handler->display->display_options['fields']['filename']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['filename']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['filename']['link_to_file'] = FALSE;
  /* Field: File Usage: Use count */
  $handler->display->display_options['fields']['count']['id'] = 'count';
  $handler->display->display_options['fields']['count']['table'] = 'file_usage';
  $handler->display->display_options['fields']['count']['field'] = 'count';
  $handler->display->display_options['fields']['count']['separator'] = '';
  /* Field: File: Name */
  $handler->display->display_options['fields']['filename_1']['id'] = 'filename_1';
  $handler->display->display_options['fields']['filename_1']['table'] = 'file_managed';
  $handler->display->display_options['fields']['filename_1']['field'] = 'filename';
  $handler->display->display_options['fields']['filename_1']['label'] = 'Find';
  $handler->display->display_options['fields']['filename_1']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['filename_1']['alter']['text'] = '<a href="/manage/vod/find-show?field_expected_filename_value=[filename]">Find</a>';
  $handler->display->display_options['fields']['filename_1']['link_to_file'] = FALSE;
  /* Field: Feeds item: Import date */
  $handler->display->display_options['fields']['imported']['id'] = 'imported';
  $handler->display->display_options['fields']['imported']['table'] = 'feeds_item';
  $handler->display->display_options['fields']['imported']['field'] = 'imported';
  $handler->display->display_options['fields']['imported']['date_format'] = 'short';
  /* Field: Feeds item: Item URL */
  $handler->display->display_options['fields']['url']['id'] = 'url';
  $handler->display->display_options['fields']['url']['table'] = 'feeds_item';
  $handler->display->display_options['fields']['url']['field'] = 'url';
  $handler->display->display_options['fields']['url']['label'] = 'URL';
  $handler->display->display_options['fields']['url']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['url']['alter']['text'] = '[<a href="[url]">Open in Cloudcast</a>]';
  $handler->display->display_options['fields']['url']['display_as_link'] = FALSE;
  /* Sort criterion: File Usage: Use count */
  $handler->display->display_options['sorts']['count']['id'] = 'count';
  $handler->display->display_options['sorts']['count']['table'] = 'file_usage';
  $handler->display->display_options['sorts']['count']['field'] = 'count';
  /* Filter criterion: File: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'file_managed';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'video' => 'video',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'manage/vod/missing-shows';
  $handler->display->display_options['menu']['type'] = 'default tab';
  $handler->display->display_options['menu']['title'] = 'VOD Without Matching Show';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'management';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['tab_options']['type'] = 'normal';
  $handler->display->display_options['tab_options']['title'] = 'VOD';
  $handler->display->display_options['tab_options']['weight'] = '0';
  $handler->display->display_options['tab_options']['name'] = 'management';
  $export['cm_vod_without_matching_show'] = $view;

  return $export;
}
